package com.pouwelse.bnd.ace.framework;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.URI;
import java.net.URL;
import java.security.cert.X509Certificate;
import java.util.Collection;
import java.util.Dictionary;
import java.util.Enumeration;
import java.util.List;
import java.util.Map;

import org.osgi.framework.Bundle;
import org.osgi.framework.BundleContext;
import org.osgi.framework.BundleException;
import org.osgi.framework.BundleListener;
import org.osgi.framework.Filter;
import org.osgi.framework.FrameworkEvent;
import org.osgi.framework.FrameworkListener;
import org.osgi.framework.InvalidSyntaxException;
import org.osgi.framework.ServiceEvent;
import org.osgi.framework.ServiceListener;
import org.osgi.framework.ServiceReference;
import org.osgi.framework.ServiceRegistration;
import org.osgi.framework.Version;
import org.osgi.framework.launch.Framework;

import com.pouwelse.bnd.ace.artifactmanager.ArtifactManager;
import com.pouwelse.bnd.ace.artifactmanager.BndAceBundle;

public class BndAceFramework implements Framework, ServiceListener {

	private AceClientLauncher client;
	
	
	private ArtifactManager artifactManager;

	public BndAceFramework(Map<String, String> configuration) {
		try {
			client = new AceClientLauncher(configuration);
			artifactManager = client.getArtifactManager();
			
			String featureName = configuration.get("com.pouwelse.bnd.ace.framework.featureName");
			
			artifactManager.init(featureName);
		} catch (Exception e) {
			System.err.println("Failed to initialize ACE client");
			throw new RuntimeException(e);
		}
	}

	@Override
	public FrameworkEvent waitForStop(long timeout) throws InterruptedException {
		return client.waitForStop(timeout);
	}

	@Override
	public void stop() throws BundleException {
		client.stop();
	}
	
	@Override
	public BundleContext getBundleContext() {
		return new BundleContext() {

			@Override
			public Bundle installBundle(String location) throws BundleException {
				try {
					location = location.replace("reference:", "");
					
					File file = new File(new URI(location));
					
					BndAceBundle bundle = artifactManager.add(file);
					
					return bundle;
				} catch (Exception e) {
					throw new BundleException("Failed to install bundle ", e);
				}
			}
			
			@Override
			public boolean ungetService(ServiceReference<?> reference) {
				// TODO Auto-generated method stub
				return false;
			}

			@Override
			public void removeServiceListener(ServiceListener listener) {
				// TODO Auto-generated method stub

			}

			@Override
			public void removeFrameworkListener(FrameworkListener listener) {
				// TODO Auto-generated method stub

			}

			@Override
			public void removeBundleListener(BundleListener listener) {
				// TODO Auto-generated method stub

			}

			@Override
			public <S> ServiceRegistration<S> registerService(Class<S> clazz, S service,
					Dictionary<String, ?> properties) {
				// TODO Auto-generated method stub
				return null;
			}

			@Override
			public ServiceRegistration<?> registerService(String clazz, Object service, Dictionary<String, ?> properties) {
				// TODO Auto-generated method stub
				return null;
			}

			@Override
			public ServiceRegistration<?> registerService(String[] clazzes, Object service,
					Dictionary<String, ?> properties) {
				// TODO Auto-generated method stub
				return null;
			}

			@Override
			public Bundle installBundle(String location, InputStream input) throws BundleException {
				// TODO Auto-generated method stub
				return null;
			}

			@Override
			public <S> Collection<ServiceReference<S>> getServiceReferences(Class<S> clazz, String filter)
					throws InvalidSyntaxException {
				// TODO Auto-generated method stub
				return null;
			}

			@Override
			public ServiceReference<?>[] getServiceReferences(String clazz, String filter)
					throws InvalidSyntaxException {
				// TODO Auto-generated method stub
				return null;
			}

			@Override
			public <S> ServiceReference<S> getServiceReference(Class<S> clazz) {
				// TODO Auto-generated method stub
				return null;
			}

			@Override
			public ServiceReference<?> getServiceReference(String clazz) {
				// TODO Auto-generated method stub
				return null;
			}

			@Override
			public <S> S getService(ServiceReference<S> reference) {
				// TODO Auto-generated method stub
				return null;
			}

			@Override
			public String getProperty(String key) {
				// TODO Auto-generated method stub
				return null;
			}

			@Override
			public File getDataFile(String filename) {
				// TODO Auto-generated method stub
				return null;
			}

			@Override
			public Bundle[] getBundles() {

				return new Bundle[0];
			}

			@Override
			public Bundle getBundle(String location) {
				// TODO Auto-generated method stub
				return null;
			}

			@Override
			public Bundle getBundle(long id) {
				// TODO Auto-generated method stub
				return null;
			}

			@Override
			public Bundle getBundle() {
				// TODO Auto-generated method stub
				return null;
			}

			@Override
			public ServiceReference<?>[] getAllServiceReferences(String clazz, String filter)
					throws InvalidSyntaxException {
				// TODO Auto-generated method stub
				return null;
			}

			@Override
			public Filter createFilter(String filter) throws InvalidSyntaxException {
				// TODO Auto-generated method stub
				return null;
			}

			@Override
			public void addServiceListener(ServiceListener listener, String filter) throws InvalidSyntaxException {
				// TODO Auto-generated method stub

			}

			@Override
			public void addServiceListener(ServiceListener listener) {
				// TODO Auto-generated method stub

			}

			@Override
			public void addFrameworkListener(FrameworkListener listener) {
				// TODO Auto-generated method stub

			}

			@Override
			public void addBundleListener(BundleListener listener) {
				// TODO Auto-generated method stub

			}
		};
	}
	
	@Override
	public int getState() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public Dictionary<String, String> getHeaders() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ServiceReference<?>[] getRegisteredServices() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ServiceReference<?>[] getServicesInUse() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public boolean hasPermission(Object permission) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public URL getResource(String name) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Dictionary<String, String> getHeaders(String locale) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Class<?> loadClass(String name) throws ClassNotFoundException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Enumeration<URL> getResources(String name) throws IOException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public long getLastModified() {
		// TODO Auto-generated method stub
		return 0;
	}
	

	
	@Override
	public void stop(int options) throws BundleException {
		
	}
	


	@Override
	public Map<X509Certificate, List<X509Certificate>> getSignerCertificates(int signersType) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Version getVersion() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public File getDataFile(String filename) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public int compareTo(Bundle o) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public void init() throws BundleException {
		// TODO Auto-generated method stub

	}


	@Override
	public void start() throws BundleException {
		// TODO Auto-generated method stub
	}

	@Override
	public void start(int options) throws BundleException {
		// TODO Auto-generated method stub

	}


	@Override
	public void uninstall() throws BundleException {
	
	}

	@Override
	public void update() throws BundleException {
		// TODO Auto-generated method stub

	}

	@Override
	public void update(InputStream in) throws BundleException {
		// TODO Auto-generated method stub

	}

	@Override
	public long getBundleId() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public String getLocation() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getSymbolicName() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Enumeration<String> getEntryPaths(String path) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public URL getEntry(String path) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Enumeration<URL> findEntries(String path, String filePattern, boolean recurse) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public <A> A adapt(Class<A> type) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void serviceChanged(ServiceEvent event) {
		// TODO Auto-generated method stub

	}

}
