package com.pouwelse.bnd.ace.framework;

import java.util.Map;

import org.osgi.framework.launch.Framework;
import org.osgi.framework.launch.FrameworkFactory;

public class BndAceFrameworkFactory implements FrameworkFactory {

	@Override
	public Framework newFramework(Map<String, String> configuration) {
		
		return new BndAceFramework(configuration);
	}
	
}
