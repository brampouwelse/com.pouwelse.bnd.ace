package com.pouwelse.bnd.ace.framework;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.StandardCopyOption;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

import org.osgi.framework.Bundle;
import org.osgi.framework.BundleContext;
import org.osgi.framework.BundleException;
import org.osgi.framework.FrameworkEvent;
import org.osgi.framework.ServiceReference;
import org.osgi.framework.launch.Framework;
import org.osgi.framework.launch.FrameworkFactory;

import aQute.launcher.constants.LauncherConstants;
import aQute.libg.classloaders.URLClassLoaderWrapper;

import com.pouwelse.bnd.ace.artifactmanager.ArtifactManager;

public class AceClientLauncher {

	private Framework framework;

	private ArtifactManager workspaceManager;

	private URL aceResourceURL;
	
	private URL aceConfiugResourceURL;

	class TestClassLoader extends ClassLoader {

		private ClassLoader urlClassLoader;

		public TestClassLoader(ClassLoader parent, ClassLoader urlClassLoader) {
			super(parent);
			this.urlClassLoader = urlClassLoader;
		}

		@Override
		public Class<?> loadClass(String name) throws ClassNotFoundException {
			try {
				return urlClassLoader.loadClass(name);
			} catch (ClassNotFoundException e) {
				return super.loadClass(name);
			}
		}

	}

	public AceClientLauncher(Map<String, String> configuration) throws Exception {

		if (configuration.containsKey("com.pouwelse.bnd.ace.framework.mode")
				&& "client".equals(configuration.get("com.pouwelse.bnd.ace.framework.mode"))) {
			aceResourceURL = AceClientLauncher.class.getClassLoader().getResource("ace-client.jar");
			aceConfiugResourceURL = AceClientLauncher.class.getClassLoader().getResource("ace-client-conf.zip");
		} else {
			aceResourceURL = AceClientLauncher.class.getClassLoader().getResource("ace-server-allinone.jar");
			aceConfiugResourceURL = AceClientLauncher.class.getClassLoader().getResource("ace-server-allinone-conf.zip");
		}

		LauncherConstants aceClientLauncherProperties = loadAceClientLauncherProperties(aceResourceURL);

		HashMap<String, String> aceClientConfiguration = getAceClientConfiguration(configuration,
				aceClientLauncherProperties);

		URL resource = toFileURL(getClass().getClassLoader().getResource("lib/org.apache.felix.framework-4.2.1.jar"));

		URLClassLoaderWrapper urlClassLoaderWrapper = new URLClassLoaderWrapper(getClass().getClassLoader());
		urlClassLoaderWrapper.addURL(resource);
		Class<?> loadClass = getClass().getClassLoader().loadClass("org.apache.felix.framework.FrameworkFactory");
		FrameworkFactory frameworkFactory = (FrameworkFactory) loadClass.newInstance();

		framework = frameworkFactory.newFramework(aceClientConfiguration);

		framework.start();

		List<Bundle> bundlesToStart = new ArrayList<>();

		BundleContext bundleContext = framework.getBundleContext();

		for (String runBundle : aceClientLauncherProperties.runbundles) {
			InputStream entry = findZipEntry(new ZipInputStream(aceResourceURL.openStream()), runBundle);
			Bundle installBundle = bundleContext.installBundle(runBundle, entry);
			bundlesToStart.add(installBundle);
			entry.close();
		}

		URL test = getClass().getClassLoader().getResource("com.pouwelse.bnd.ace.artifactmanager.impl.jar");
		Bundle installBundle = bundleContext.installBundle("com.pouwelse.bnd.ace.artifactmanager.impl.jar",
				test.openStream());
		bundlesToStart.add(installBundle);

		try {
			for (Bundle bundle : bundlesToStart) {
				bundle.start();
			}
		} catch (Error | Exception e) {
			e.printStackTrace();
		}

		ServiceReference<ArtifactManager> serviceReference = null;
		for (int i = 0; i < 10; i++) {
			serviceReference = bundleContext.getServiceReference(ArtifactManager.class);
			if (serviceReference != null) {
				workspaceManager = bundleContext.getService(serviceReference);
				break;
			}

			Thread.sleep(200l);
		}

		if (workspaceManager == null) {
			throw new RuntimeException("WorkspaceManager service not available");

		}
	}

	private LauncherConstants loadAceClientLauncherProperties(URL aceClientJar) throws IOException {
		InputStream launcherProperties = findZipEntry(new ZipInputStream(aceClientJar.openStream()),
				"launcher.properties");
		Properties properties = new Properties();
		properties.load(launcherProperties);
		LauncherConstants aceClientLauncherProperties = new LauncherConstants(properties);
		return aceClientLauncherProperties;
	}

	private HashMap<String, String> getAceClientConfiguration(Map<String, String> configuration,
			LauncherConstants launcherConstants) throws IOException {
		HashMap<String, String> config = new HashMap<>(launcherConstants.runProperties);
		config.put("org.osgi.framework.storage.clean", "onFirstInit");

		if (configuration.containsKey("org.apache.ace.server")) {
			config.put("org.apache.ace.server", configuration.get("org.apache.ace.server"));
		} else {
			config.put("org.apache.ace.server", "localhost:8080");
		}

		if (configuration.containsKey("org.osgi.service.http.port")) {
			config.put("org.osgi.service.http.port", configuration.get("org.osgi.service.http.port"));
		} else {
			config.put("org.osgi.service.http.port", "localhost:8080");
		}

		if (configuration.containsKey("org.apache.ace.obr")) {
			config.put("org.apache.ace.obr", configuration.get("org.apache.ace.obr"));
		} else {
			config.put("org.apache.ace.obr", "localhost:8080");
		}

		config.put("org.osgi.framework.system.packages.extra", "com.pouwelse.bnd.ace.artifactmanager;version=\"1.0\"");

		String storageBase;
		if (configuration.containsKey("launch.storage.dir")) {
			storageBase = configuration.get("launch.storage.dir").concat("/");
		} else {
			storageBase = "bnd-ace-framework-storage/";
		}

		config.put(org.osgi.framework.Constants.FRAMEWORK_STORAGE,
				storageBase.concat(launcherConstants.storageDir.getName()));

		if (configuration.containsKey("org.apache.ace.configurator.CONFIG_DIR")) {
			config.put("org.apache.ace.configurator.CONFIG_DIR",
					configuration.get("org.apache.ace.configurator.CONFIG_DIR"));
		} else {
			extractDefaultAceClientConfiguration(storageBase);

			String configDir = storageBase.concat("conf");
			config.put("org.apache.ace.configurator.CONFIG_DIR", configDir);
		}

		return config;
	}

	private void extractDefaultAceClientConfiguration(String configDir) throws IOException {
		File target = new File(configDir);

		ZipInputStream zipInputStream = new ZipInputStream(aceConfiugResourceURL.openStream());
		ZipEntry e;
		while ((e = zipInputStream.getNextEntry()) != null) {
			File file = new File(target, e.getName());
			if (e.isDirectory()) {
				file.mkdirs();
			} else {
				Files.copy(zipInputStream, file.toPath(), StandardCopyOption.REPLACE_EXISTING);
			}
		}
	}

	private InputStream findZipEntry(ZipInputStream in, String name) throws IOException {
		ZipEntry entry;
		while ((entry = in.getNextEntry()) != null) {
			if (entry.getName().equals(name)) {
				return in;
			}
		}
		throw new RuntimeException("Entry not found ");
	}

	public FrameworkEvent waitForStop(long timeout) throws InterruptedException {
		return framework.waitForStop(timeout);
	}

	public void stop() throws BundleException {
		framework.stop();
	}

	private static URL toFileURL(URL resource) throws IOException {
		File f = File.createTempFile("resource", ".jar");
		f.getParentFile().mkdirs();
		InputStream in = resource.openStream();
		byte[] buffer = new byte[4096];
		try {
			OutputStream out = new FileOutputStream(f);
			try {
				int size = in.read(buffer);
				while (size > 0) {
					out.write(buffer, 0, size);
					size = in.read(buffer);
				}
			} finally {
				out.close();
			}
		} finally {
			in.close();
		}
		f.deleteOnExit();
		return f.toURI().toURL();
	}

	public ArtifactManager getArtifactManager() {
		return workspaceManager;
	}

}
