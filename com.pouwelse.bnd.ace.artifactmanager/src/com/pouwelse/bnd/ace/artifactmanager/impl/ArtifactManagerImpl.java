package com.pouwelse.bnd.ace.artifactmanager.impl;

import static org.apache.ace.gogo.repo.RepositoryUtil.getIdentity;
import static org.apache.ace.gogo.repo.RepositoryUtil.getVersion;
import static org.apache.ace.gogo.repo.DeployerUtil.isSameBaseVersion;
import static org.apache.ace.gogo.repo.DeployerUtil.getNextSnapshotVersion;
import static org.apache.ace.gogo.repo.DeployerUtil.getBundleWithNewVersion;

import java.io.File;
import java.io.FileInputStream;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;

import org.apache.ace.bnd.repository.AceObrRepository;
import org.apache.ace.client.repository.object.Artifact2FeatureAssociation;
import org.apache.ace.client.repository.object.ArtifactObject;
import org.apache.ace.client.repository.object.Distribution2TargetAssociation;
import org.apache.ace.client.repository.object.DistributionObject;
import org.apache.ace.client.repository.object.Feature2DistributionAssociation;
import org.apache.ace.client.repository.object.FeatureObject;
import org.apache.ace.client.workspace.Workspace;
import org.apache.ace.client.workspace.WorkspaceManager;
import org.apache.ace.gogo.repo.DeployerUtil;
import org.apache.ace.gogo.repo.RepositoryUtil;
import org.osgi.framework.BundleContext;
import org.osgi.framework.Version;
import org.osgi.resource.Resource;

import aQute.bnd.osgi.Jar;
import aQute.bnd.service.RepositoryPlugin.PutResult;
import aQute.bnd.service.Strategy;

import com.pouwelse.bnd.ace.artifactmanager.ArtifactManager;
import com.pouwelse.bnd.ace.artifactmanager.BndAceBundle;

public class ArtifactManagerImpl implements ArtifactManager {

	private volatile WorkspaceManager workspaceManager;

	private volatile BundleContext bundleContext;

	private String featureName;

	private ScheduledExecutorService executor = Executors.newSingleThreadScheduledExecutor();

	private ScheduledFuture<?> scheduledFuture;

	private Object lock = new Object();

	private Set<BndAceBundle> bundlesToAdd = new HashSet<>();
	private Set<BndAceBundle> bundlesToRemove = new HashSet<>();

	private AceObrRepository aceRepository;

	@SuppressWarnings("unused" /* dependency manager callback */)
	private void init() {
		aceRepository = new AceObrRepository();
		Map<String, String> props = new HashMap<String, String>();
		props.put(AceObrRepository.PROP_LOCATIONS,
				String.format("http://%s/obr/repository.xml", bundleContext.getProperty("org.apache.ace.obr")));
		aceRepository.setProperties(props);
	}

	@Override
	public void init(String featureName) {
		this.featureName = featureName;
		prepareFeature();
	}

	private void prepareFeature() {
		try {
			Workspace workspace = workspaceManager.cw();

			List<FeatureObject> lf = workspace.lf(String.format("(name=%s)", featureName));
			if (lf.isEmpty()) {
				workspace.cf(featureName);
			} else {
				List<Artifact2FeatureAssociation> la2f = workspace.la2f(String.format("(rightEndpoint=\\(name=%s\\))",
						featureName));
				for (Artifact2FeatureAssociation artifact2FeatureAssociation : la2f) {
					workspace.da2f(artifact2FeatureAssociation);
				}
			}

			List<DistributionObject> ld = workspace.ld(String.format("(name=%s)", featureName));
			if (ld.isEmpty()) {
				workspace.cd(featureName);
				List<Feature2DistributionAssociation> lf2d = workspace.lf2d(String.format(
						"(leftEndpoint=\\(name=%s\\))", featureName));
				if (lf2d.isEmpty()) {
					workspace.cf2d(String.format("(name=%s)", featureName), String.format("(name=%s)", featureName));
				}
			}
			
			List<Distribution2TargetAssociation> ld2t = workspace.ld2t(String.format("(leftEndpoint=\\(name=%s\\))", featureName));
			if (ld2t.isEmpty()){
				workspace.cd2t(String.format("(name=%s)", featureName), "(id=auto-*)");
			}

			workspace.commit();
			workspaceManager.rw(workspace);

		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	@Override
	public BndAceBundle add(File file) throws Exception {
		try (Jar jar = new Jar(file)) {
			String bsn = jar.getBsn();
			Version version = new Version(jar.getVersion());
			BndAceBundle bundle = new BndAceBundle(this, file, bsn, version);
			synchronized (lock) {
				bundlesToAdd.add(bundle);
				processChanges();
			}
			return bundle;
		}
	}

	@Override
	public void update(BndAceBundle bundle) {
		synchronized (lock) {
			bundlesToAdd.add(bundle);
			bundlesToRemove.remove(bundle);
			processChanges();
		}
	}

	@Override
	public void remove(BndAceBundle bundle) {
		synchronized (lock) {
			bundlesToAdd.remove(bundle);
			bundlesToRemove.add(bundle);
			processChanges();
		}

	}

	private void processChanges() {

		if (scheduledFuture != null && !scheduledFuture.isDone()) {
			scheduledFuture.cancel(false);
		}

		scheduledFuture = executor.schedule(new Runnable() {

			@Override
			public void run() {
				synchronized (lock) {
					try {
						Workspace workspace = workspaceManager.cw();

						for (BndAceBundle bundle : bundlesToAdd) {
							Version base = bundle.getVersion();

							List<org.osgi.resource.Resource> resources = RepositoryUtil.findResources(aceRepository,
									bundle.getSymbolicName());
							Resource matchedResource = null;
							for (Resource candidateResource : resources) {
								if (isSameBaseVersion(getVersion(candidateResource), base)
										&& (matchedResource == null || getVersion(matchedResource).compareTo(
												getVersion(candidateResource)) < 0)) {
									matchedResource = candidateResource;
								}
							}

							String location = null;
							String version = null;
							if (matchedResource == null) {
								System.out.println(String.format("Install %s(%s)", bundle.getSymbolicName(),
										bundle.getVersion()));

								PutResult put = aceRepository.put(new FileInputStream(bundle.getFile()), null);
								location = put.artifact.toString();
								version = bundle.getVersion().toString();
							} else {
								File repoFile = aceRepository.get(getIdentity(matchedResource),
										getVersion(matchedResource).toString(), Strategy.EXACT, null);

								Version nextVersion = getNextSnapshotVersion(getVersion(matchedResource));
								File bundleWithNewVersion = getBundleWithNewVersion(bundle.getFile(),
										nextVersion.toString());
								File repoFileWithNextVersion = getBundleWithNewVersion(repoFile, nextVersion.toString());

								if (DeployerUtil.jarsDiffer(bundleWithNewVersion, repoFileWithNextVersion)) {
									System.out.println(String.format("Update %s(%s -> %s)", bundle.getSymbolicName(),
											bundle.getVersion(), nextVersion));
									PutResult put = aceRepository.put(new FileInputStream(bundleWithNewVersion), null);
									location = put.artifact.toString();
									version = nextVersion.toString();
								}
								bundleWithNewVersion.delete();
								repoFileWithNextVersion.delete();

							}

							if (version != null) {
								String artifactName = String.format("%s-%s", bundle.getSymbolicName(), version);
								workspace.ca(artifactName.toString(), location, bundle.getSymbolicName(), version);
							}else {
								String artifactName = String.format("%s-%s", bundle.getSymbolicName(), base);
								List<ArtifactObject> la = workspace.la(String.format("(name=%s)", artifactName));
								if (la.isEmpty()){
									workspace.ca(artifactName.toString(), location, bundle.getSymbolicName(), base.toString());	
								}
							}

							List<Artifact2FeatureAssociation> la2f = workspace.la2f(String.format(
									"(&(rightEndpoint=\\(name=%s\\))(leftEndpoint=\\(Bundle-SymbolicName=%s\\)))",
									featureName, bundle.getSymbolicName()));

							if (la2f.isEmpty()) {
								workspace.ca2f(String.format("(Bundle-SymbolicName=%s)", bundle.getSymbolicName()),
										String.format("(name=%s)", featureName));
							}
						}
						bundlesToAdd.clear();

						for (BndAceBundle bundle : bundlesToRemove) {
							System.out.println(String.format("Removing %s(%s)", bundle.getSymbolicName(),
									bundle.getVersion()));
							List<Artifact2FeatureAssociation> la2f = workspace.la2f(String.format(
									"(&(rightEndpoint=\\(name=%s\\))(leftEndpoint=\\(Bundle-SymbolicName=%s\\)))",
									featureName, bundle.getSymbolicName()));

							for (Artifact2FeatureAssociation artifact2FeatureAssociation : la2f) {
								workspace.da2f(artifact2FeatureAssociation);
							}
						}
						bundlesToRemove.clear();

						workspace.commit();
						workspaceManager.rw(workspace);
					} catch (Error | Exception e) {
						e.printStackTrace();
					}
				}

			}
		}, 300, TimeUnit.MILLISECONDS);

	}

}
