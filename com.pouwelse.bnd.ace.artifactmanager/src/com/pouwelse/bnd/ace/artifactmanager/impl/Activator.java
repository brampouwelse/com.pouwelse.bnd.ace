package com.pouwelse.bnd.ace.artifactmanager.impl;

import org.apache.ace.client.workspace.WorkspaceManager;
import org.apache.felix.dm.DependencyActivatorBase;
import org.apache.felix.dm.DependencyManager;
import org.osgi.framework.BundleContext;

import com.pouwelse.bnd.ace.artifactmanager.ArtifactManager;

public class Activator extends DependencyActivatorBase {

	@Override
	public void destroy(BundleContext arg0, DependencyManager arg1) throws Exception {

	}

	@Override
	public void init(BundleContext arg0, DependencyManager dm) throws Exception {
		dm.add(createComponent().setInterface(ArtifactManager.class.getName(), null)
				.setImplementation(ArtifactManagerImpl.class)
				.add(createServiceDependency().setService(WorkspaceManager.class).setRequired(true)));

	}

}
