package com.pouwelse.bnd.ace.artifactmanager;

import java.io.File;

public interface ArtifactManager {
	
	public void init(String workspaceName);

	public BndAceBundle add(File file) throws Exception;
	
	public void update(BndAceBundle bndAceBundle);
	
	public void remove(BndAceBundle bndAceBundle);
}
